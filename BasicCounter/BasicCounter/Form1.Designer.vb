﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Plus = New System.Windows.Forms.Button()
        Me.Moins = New System.Windows.Forms.Button()
        Me.Total = New System.Windows.Forms.Label()
        Me.RAZ = New System.Windows.Forms.Button()
        Me.Score = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Plus
        '
        Me.Plus.Location = New System.Drawing.Point(525, 202)
        Me.Plus.Name = "Plus"
        Me.Plus.Size = New System.Drawing.Size(225, 42)
        Me.Plus.TabIndex = 0
        Me.Plus.Text = "+"
        Me.Plus.UseVisualStyleBackColor = True
        '
        'Moins
        '
        Me.Moins.Location = New System.Drawing.Point(52, 202)
        Me.Moins.Name = "Moins"
        Me.Moins.Size = New System.Drawing.Size(209, 42)
        Me.Moins.TabIndex = 1
        Me.Moins.Text = "-"
        Me.Moins.UseVisualStyleBackColor = True
        '
        'Total
        '
        Me.Total.Location = New System.Drawing.Point(365, 77)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(52, 18)
        Me.Total.TabIndex = 2
        Me.Total.Text = "Total"
        Me.Total.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'RAZ
        '
        Me.RAZ.Location = New System.Drawing.Point(306, 279)
        Me.RAZ.Name = "RAZ"
        Me.RAZ.Size = New System.Drawing.Size(187, 75)
        Me.RAZ.TabIndex = 3
        Me.RAZ.Text = "0"
        Me.RAZ.UseVisualStyleBackColor = True
        '
        'Score
        '
        Me.Score.Location = New System.Drawing.Point(336, 148)
        Me.Score.Name = "Score"
        Me.Score.Size = New System.Drawing.Size(127, 77)
        Me.Score.TabIndex = 4
        Me.Score.Text = "0"
        Me.Score.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Score)
        Me.Controls.Add(Me.RAZ)
        Me.Controls.Add(Me.Total)
        Me.Controls.Add(Me.Moins)
        Me.Controls.Add(Me.Plus)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Plus As Button
    Friend WithEvents Moins As Button
    Friend WithEvents Total As Label
    Friend WithEvents RAZ As Button
    Friend WithEvents Score As Label
End Class
