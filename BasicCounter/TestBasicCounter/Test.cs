﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeBasicCounter;

namespace TestBasicCounter
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestIncrementresultatcreated()
        {
            Assert.AreEqual(1,CodeBasicCounter.Code.Incrementresultatcreated(0));
            Assert.AreEqual(5, CodeBasicCounter.Code.Incrementresultatcreated(4));
        }

        [TestMethod]
        public void TestDesincrementresultatcreated()
        {
            Assert.AreEqual(1, CodeBasicCounter.Code.Desincrementresultatcreated(2));
            Assert.AreEqual(5, CodeBasicCounter.Code.Desincrementresultatcreated(6));
        }

        [TestMethod]
        public void TestResetresultat()
        {
            Assert.AreEqual(0, CodeBasicCounter.Code.Resetresultat(5));
            Assert.AreEqual(0, CodeBasicCounter.Code.Resetresultat(45));
        }
    }
}
